mod count_primes;
use crate::count_primes::count_primes;
use primes::is_prime;

fn main() {
    let n = 4000000;
     if is_prime(n) { println!("{} is a prime", n) } else { println!("{} is not a prime", n) };
    count_primes(n);
}

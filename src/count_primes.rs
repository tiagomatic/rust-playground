extern crate primes;

use primes::is_prime;

pub fn count_primes(n :u64) {
    let mut x :u64 = 0;
    for _i in 2..n+1 {
        if is_prime(_i) {
            x += 1
        }
    }
    println!("{} primes precede {}", x,n);
}
